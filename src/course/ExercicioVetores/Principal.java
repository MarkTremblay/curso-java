package course.ExercicioVetores;

import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        Quartos[] quartos = new Quartos[10];

        System.out.print("Quantos quartos serão alugados?");
        int quantos = in.nextInt();

        for (int i = 1; i <= quantos; i++) {

            System.out.println("Venda "+i);

            in.nextLine();
            System.out.print("Nome: ");
            String nome = in.nextLine();

            System.out.print("Email: ");
            String email = in.nextLine();

            System.out.print("Quarto: ");
            int quarto = in.nextInt();

            quartos[quarto] = new Quartos(nome, email, quarto);

        }

        System.out.println("\nQuartos ocupados:");

        for (int i = 0; i < quartos.length; i++){

            if(quartos[i] != null) {

                System.out.println(quartos[i].getQuarto()+": "+quartos[i].getNome()+", "+quartos[i].getEmail());
            }
        }
    }
}
