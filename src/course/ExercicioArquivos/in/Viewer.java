package course.ExercicioArquivos.in;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Viewer {

    public static void main(String[] args){

        try {

            String         caminho    = "D:\\Documentos\\Java\\Curso Java\\Projeto\\src\\course\\ExercicioArquivos\\in\\in.txt";
            BufferedReader lerArquivo = new BufferedReader(new FileReader(caminho));
            String         linha      = lerArquivo.readLine();
            List <Product> lista      = new ArrayList <>();
            while (linha != null) {

                String[] conteudo = linha.split(",");

                Product produto = new Product(
                        conteudo[0],
                        Double.parseDouble(conteudo[1]),
                        Integer.parseInt(conteudo[2])
                );

                lista.add(produto);

                linha = lerArquivo.readLine();
            }

            lerArquivo.close();

            String         caminhoFinal   = "D:\\Documentos\\Java\\Curso Java\\Projeto\\src\\course\\ExercicioArquivos\\out\\summary.csv";
            BufferedWriter escreveArquivo = new BufferedWriter(new FileWriter(caminhoFinal));

            for (Product c : lista) {
                escreveArquivo.write(c.getName() + "," + String.format("%.2f", c.total()));
                escreveArquivo.newLine();
            }

            escreveArquivo.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
