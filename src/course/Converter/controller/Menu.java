package course.Converter.controller;

import course.Converter.model.Pokemon;

import java.util.Scanner;

import static course.Converter.controller.Pokedex.lista;

public class Menu {

    public static Scanner in = new Scanner(System.in);

    public static void pesquisa(){

        int entrada;

        do {
            System.out.print("\n1: Pesquisa global    2: Pesquisa filtrada    0: Voltar" +
                             "\nDigite uma das opções acima: ");
            entrada = in.nextInt();

            switch ( entrada ) {

                case 1:
                    in.nextLine();
                    System.out.print("\nDigite qualquer informação para pesquisar: ");
                    String informacao = in.nextLine();

                    for (Pokemon item : lista) {

                        if (item.toString().contains(informacao)) {
                            System.out.println(item.toString());
                        }
                    }

                    break;

                case 2:
                    pesquisaFiltrada();
                    break;

                case 0:
                default:
                    System.out.println("Entrada inválida, tente novamente.");
            }
        } while (entrada != 0);
    }

    public static void pesquisaFiltrada(){

        int opcao;

        do {
            System.out.print("\nPesquisar por:" +
                             "\n1: Número    2: Nome    3: Tipo    4: Categoria    5: Altura    6: Peso    7: Geração    0: Voltar" +
                             "\nDigite uma das opções acima: ");
            opcao = in.nextInt();

            switch ( opcao ) {
                case 1: //por número

                    System.out.print("\nDigite o número do pokémon: ");
                    int numero = in.nextInt();

                    for (Pokemon item : lista) {

                        if (item.getNumero() == numero) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 2: //por nome

                    in.nextLine();
                    System.out.print("\nDigite o nome do pokémon: ");
                    String nome = in.nextLine();

                    for (Pokemon item : lista) {

                        if (item.getNome().contains(nome)) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 3: //por tipo

                    in.nextLine();
                    System.out.print("\nDigite o tipo do pokémon: ");
                    String tipo = in.nextLine();

                    for (Pokemon item : lista) {

                        if (item.getTipo().contains(tipo)) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 4: //por categoria

                    in.nextLine();
                    System.out.print("\nDigite a categoria do pokémon: ");
                    String categoria = in.nextLine();

                    for (Pokemon item : lista) {

                        if (item.getCategoria().contains(categoria)) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 5: //por altura

                    System.out.print("\nDigite a altura do pokémon usando vígulas se nescessário: ");
                    double altura = in.nextDouble();

                    for (Pokemon item : lista) {

                        if (item.getAltura() == altura) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 6: //por peso

                    System.out.print("\nDigite o peso do pokémon usando vígulas se nescessário: ");
                    double peso = in.nextDouble();

                    for (Pokemon item : lista) {

                        if (item.getPeso() == peso) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 7: //por geração

                    System.out.print("\nDigite a geração do pokémon: ");
                    int geracao = in.nextInt();

                    for (Pokemon item : lista) {

                        if (item.getGeracao() == geracao) {
                            System.out.println(item.toString());
                        }
                    }
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Entrada inválida, tente novamente.");
            }

        } while (opcao != 0);
    }

    public static void catalogo() throws Exception{

        int entrada;

        do {
            System.out.print("\n1: Listar    2: Atualizar    0: Voltar" +
                             "\nDigite uma das opções acima: ");
            entrada = in.nextInt();

            switch ( entrada ) {
                case 1:
                    Pokedex.mostrarLista();
                    break;

                case 2:
                    Pokedex.atualizarLista();
                    System.out.println("Atualizada.");
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Opção inválida, tente novamente.");
            }
        } while (entrada != 0);
    }

    public static void pokemons() throws Exception{

        int entrada;

        do {
            System.out.print("\n1: Adicionar   2: Remover   3: Alterar   0: Voltar" +
                             "\nDigite uma das opções acima: ");
            entrada = in.nextInt();

            switch ( entrada ) {
                case 1: //adicionar

                    adicionar();
                    break;

                case 2: //remover

                    remover();
                    break;

                case 3: //alterar

                    alterar();
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Opção inválida, tente novamente.");
            }

        } while (entrada != 0);
    }

    public static void adicionar() throws Exception{

        System.out.print("\nNúmero: ");
        int numero = in.nextInt();

        in.nextLine();
        System.out.print("Nome: ");
        String nome = in.nextLine();

        System.out.print("Tipo: ");
        String tipo = in.nextLine();

        System.out.print("Categoria: ");
        String categoria = in.nextLine();

        System.out.print("Altura: ");
        double altura = in.nextDouble();

        System.out.print("Peso: ");
        double peso = in.nextDouble();

        System.out.print("Geração: ");
        int geracao = in.nextInt();

        Pokemon pokemon = new Pokemon(numero, nome, tipo, categoria, altura, peso, geracao);
        System.out.println(pokemon.toString());

        for (Pokemon item : lista) {
            if (item.getNumero() == numero) {
                System.out.println("Já existe um pokémon com o número " + item.getNumero() + " e ele se chama " + item.getNome() + ".");
                System.out.println("Se você optar por salvar mesmo assim, todos os pokémons incluindo " + item.getNome() + " terão" +
                                   " seus números incrementados para ficarem de acordo com a nova posição deles no catálogo.");
            }
        }

        for (Pokemon item : lista) {
            if (item.equals(pokemon)) {
                System.out.println("Aparentemente este pokémon já existe no catálogo," +
                                   " se você optar por salvar mesmo assim, todos os pokémons incluindo este mesmo terão " +
                                   "seus números incrementados para ficarem de acordo com a nova posição deles no catálogo.");
            }
        }

        int entrada;

        do {
            System.out.print("\nSalvar?:" +
                             "\t1: Sim    2: Não" +
                             "\nDigite uma das opções acima: ");
            entrada = in.nextInt();

            switch ( entrada ) {

                case 1:

                    lista.add(pokemon.getNumero() - 1, pokemon);
                    Pokedex.salvarLista();
                    break;

                case 2:
                    break;

                default:
                    System.out.println("Opção inválida, tente novamente.");
            }

        }
        while (entrada != 1 && entrada != 2);

    }

    public static void remover() throws Exception{

        System.out.print("Digite o número do pokémon: ");
        int numero = in.nextInt();

        in.nextLine();
        System.out.print("Digite o nome do pokémon: ");
        String nome = in.nextLine();

        Pokemon pokemon = new Pokemon();

        for (Pokemon item : lista) {
            if (item.getNome().equals(nome) && item.getNumero() == numero) {
                pokemon = item;
            }
        }
        lista.remove(pokemon);

        Pokedex.salvarLista();
    }

    public static void alterar() throws Exception{

        System.out.print("Digite o número do pokémon: ");
        int numero = in.nextInt();

        in.nextLine();
        System.out.print("Digite o nome do pokémon: ");
        String nome = in.nextLine();

        Pokemon pokemon = new Pokemon();

        for (Pokemon item : lista) {

            if (item.getNome().equals(nome) && item.getNumero() == numero) {
                pokemon = new Pokemon(item.getNumero(), item.getNome(), item.getTipo(), item.getCategoria(), item.getAltura(), item.getPeso(), item.getGeracao());
                break;
            }
        }

        int entrada;

        do {
            System.out.println(pokemon.toString());
            System.out.print("\nAlterar" +
                             "\n1: Número    2: Nome    3: Tipo    4: Categoria    5: Altura6:    Peso    7: Geração" +
                             "\n8: Salvar    0: Voltar" +
                             "\nDigite uma das opções: ");
            entrada = in.nextInt();

            switch ( entrada ) {

                case 1: //número

                    System.out.print("Digite o novo número: ");
                    int numeroNovo = in.nextInt();
                    pokemon.setNumero(numeroNovo);
                    break;

                case 2: //nome

                    in.nextLine();
                    System.out.print("Digite o novo nome: ");
                    String nomeNovo = in.nextLine();
                    pokemon.setNome(nomeNovo);
                    break;

                case 3: //tipo

                    in.nextLine();
                    System.out.print("Digite o novo tipo: ");
                    String tipoNovo = in.nextLine();
                    pokemon.setTipo(tipoNovo);
                    break;

                case 4: //categoria

                    in.nextLine();
                    System.out.print("Digite a nova categoria: ");
                    String categoriaNova = in.nextLine();
                    pokemon.setCategoria(categoriaNova);
                    break;

                case 5: //altura

                    System.out.print("Digite a nova altura: ");
                    double alturaNovo = in.nextDouble();
                    pokemon.setAltura(alturaNovo);
                    break;

                case 6: //peso

                    System.out.print("Digite o novo peso: ");
                    double pesoNovo = in.nextDouble();
                    pokemon.setPeso(pesoNovo);
                    break;

                case 7: //geração

                    System.out.print("Digite a nova geração: ");
                    int geracaoNovo = in.nextInt();
                    pokemon.setGeracao(geracaoNovo);
                    break;

                case 8: //salvar

                    pokemonsSalvar(pokemon, numero, nome);
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Opção inválida, tente novamente.");
            }

        } while (entrada != 0);
    }

    public static void pokemonsSalvar(Pokemon pokemon, int numero, String nome) throws Exception{

        System.out.println(pokemon.toString());

        for (Pokemon item : lista) {
            if (item.getNumero() == pokemon.getNumero()) {
                System.out.println("Já existe um pokémon com o número " + item.getNumero() + " e ele se chama " + item.getNome() + ".");
                System.out.println("Se você optar por salvar mesmo assim, todos os pokémons incluindo " + item.getNome() + " terão" +
                                   " seus números alterados para ficarem de acordo com a nova posição deles no catálogo.\n");
            }
        }

        for (Pokemon item : lista) {
            if (item.equals(pokemon)) {
                System.out.println("Aparentemente este pokémon já existe no catálogo," +
                                   " se você optar por salvar mesmo assim, todos os pokémons incluindo este mesmo terão " +
                                   "seus números alterados para ficarem de acordo com a nova posição deles no catálogo.\n");
            }
        }

        int entrada2;
        do {
            System.out.print("\nSalvar?:" +
                             "\n1: Sim    2: Não" +
                             "\nDigite uma opção:");
            entrada2 = in.nextInt();

            switch ( entrada2 ) {

                case 1:

                    Pokemon aux = new Pokemon();

                    for (Pokemon item : lista) {
                        if (item.getNumero() == numero && item.getNome().equals(nome)) {
                            aux = item;
                        }
                    }

                    lista.remove(aux);
                    lista.add(pokemon.getNumero() - 1, pokemon);

                    Pokedex.salvarLista();
                    Pokedex.atualizarLista();

                    break;

                case 2:
                    break;

                default:
                    System.out.println("Opção inválida, tente novamente.");
            }

        }
        while (entrada2 != 1 && entrada2 != 2);
    }
}
