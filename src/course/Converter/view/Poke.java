package course.Converter.view;

import course.Converter.controller.Menu;
import course.Converter.controller.Pokedex;

import java.util.Scanner;

public class Poke {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws Exception{

        Pokedex.atualizarLista();
        int entrada;

        do {

            System.out.print(
                    "\nPOKÉDEX" +
                    "\n1: Pesquisa    2: Catálogo    3: Pokémons    0: Sair" +
                    "\nDigite uma das opções acima: ");
            entrada = in.nextInt();

            switch ( entrada ) {

                case 1: //pesquisa

                    Menu.pesquisa();
                    break;

                case 2: //catálogo

                    Menu.catalogo();
                    break;

                case 3: //pokémons

                    Menu.pokemons();
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Entrada inválida, tente de novo.");
            }
        }
        while (entrada != 0);
    }
}
