package course.Menu.Aplication;

import course.Menu.Entities.GerarRelatorio;
import java.util.Scanner;

public class Menu {

    public static Scanner in = new Scanner(System.in);

    public static void principal() {

        int entrada;

        do {
            final int abrir_conta      = 1;
            final int selecionar_conta = 2;
            final int gerar_relatorio  = 3;
            final int sair             = 4;

            System.out.print("\n+-------------------------+" +
                             "\nMenu principal do banco:" +
                             "\n1: Abrir conta." +
                             "\n2: Selecionar conta." +
                             "\n3: Gerar relatório." +
                             "\n4: Sair" +
                             "\nDigite sua opção:"
            );
            entrada = in.nextInt();

            switch ( entrada ) {

                case abrir_conta:
                    Menu.abrir_conta();
                    break;

                case selecionar_conta:
                    Menu.selecionar_conta();
                    break;

                case gerar_relatorio:
                    GerarRelatorio.relatorio();
                    break;

                case sair:
                    break;

                default:
                    System.out.println("\nDigite uma opção válida.");
            }

        } while (entrada != 4);
    }

    public static void abrir_conta() {

        int entrada;

        do {
            final int conta_poupanca = 1;
            final int conta_corrente = 2;
            final int voltar         = 3;

            System.out.print("\n+------------------------+" +
                             "\nAbrir conta:" +
                             "\n1: Conta poupança." +
                             "\n2: Conta corrente." +
                             "\n3: Voltar." +
                             "\nDigite sua opção:"
            );
            entrada = in.nextInt();

            switch ( entrada ) {

                case conta_poupanca:
                    System.out.println("\nCriada poupança.");
                    break;

                case conta_corrente:
                    System.out.println("\nCrida corente.");

                case voltar:
                    break;

                default:
                    System.out.println("\nDigite uma opção válida.");
            }

        } while (entrada != 3);
    }

    public static void selecionar_conta() {

        int entrada;

        do {
            final int sacar           = 1;
            final int depositar       = 2;
            final int gerar_relatorio = 3;
            final int remover_conta   = 4;
            final int voltar          = 5;

            System.out.print("\n+------------------------+" +
                             "\nSelecionar conta:" +
                             "\n1: Sacar." +
                             "\n2: Depositar." +
                             "\n3: Gerar relatório." +
                             "\n4: Remover conta." +
                             "\n5: Voltar." +
                             "\nDigite sua opção:"
            );
            entrada = in.nextInt();

            switch ( entrada ) {

                case sacar:
                    System.out.println("\nSacado.");
                    break;

                case depositar:
                    System.out.println("\nDepositado.");
                    break;

                case gerar_relatorio:
                    GerarRelatorio.relatorio();
                    break;

                case remover_conta:
                    System.out.println("\nRemovida.");
                    break;

                case voltar:
                    break;

                default:
                    System.out.println("\nDigite uma opção válida.");
            }

        } while (entrada != 5);
    }
}