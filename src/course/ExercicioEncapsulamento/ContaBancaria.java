package course.ExercicioEncapsulamento;

public class ContaBancaria {

    private int numeroDaConta;
    private String nomeDoTitular;
    private double saldoDaConta;

    public ContaBancaria() {
    }

    public ContaBancaria(int numeroDaConta, String nomeDoTitular, double valorDeDeposito) {
        this.numeroDaConta = numeroDaConta;
        this.nomeDoTitular = nomeDoTitular;
        this.saldoDaConta = valorDeDeposito;
    }

    public ContaBancaria(int numeroDaConta, String nomeDoTitular) {
        this.numeroDaConta = numeroDaConta;
        this.nomeDoTitular = nomeDoTitular;
    }

    public int getNumeroDaConta() {
        return numeroDaConta;
    }

    public String getNomeDoTitular() {
        return nomeDoTitular;
    }

    public double getSaldoDaConta() {
        return saldoDaConta;
    }

    public void setNomeDoTitular(String nomeDoTitular) {
        this.nomeDoTitular = nomeDoTitular;
    }

    public void depositar(double quantia) {
        saldoDaConta += quantia;
    }

    public void sacar(double quantia) {
        saldoDaConta -= quantia + 5;
    }

    @Override
    public String toString() {
        return String.format("Account %d, Holder: %s, Balance: $%.2f",numeroDaConta,nomeDoTitular,saldoDaConta);
    }
}
