package course.ExercicioEncapsulamento;

import java.util.Scanner;

public class Programa {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        ContaBancaria conta = new ContaBancaria();

        System.out.print("Enter account number: ");
        int accountNumber = in.nextInt();

        in.nextLine();
        System.out.print("Enter account holder: ");
        String accountHolder = in.nextLine();

        System.out.print("Is there an initial deposit (y/n)?");
        char answer = in.next().charAt(0);

        if (answer == 'y') {
            System.out.print("Enter initial deposit value: ");
            double deposit = in.nextDouble();

            conta = new ContaBancaria(accountNumber,accountHolder,deposit);
        }
        else if (answer == 'n') {
            conta = new ContaBancaria(accountNumber,accountHolder);
        }

        System.out.println("\nAccount data:");
        System.out.println(conta.toString());

        System.out.print("\nEnter a deposit value: ");
        double value = in.nextDouble();
        conta.depositar(value);

        System.out.println("Updated account data: ");
        System.out.println(conta.toString());

        System.out.print("\nEnter a withdraw value: ");
        double withdraw = in.nextDouble();
        conta.sacar(withdraw);

        System.out.println("Updated account data: ");
        System.out.println(conta.toString());

        in.close();
    }
}
