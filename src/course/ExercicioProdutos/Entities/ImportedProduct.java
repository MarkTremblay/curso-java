package course.ExercicioProdutos.Entities;

public class ImportedProduct extends Product {

    private double customs_fee;

    public ImportedProduct() {}

    public ImportedProduct(String name, double price, double customs_fee){
        super(name, price);
        this.customs_fee = customs_fee;
    }

    public double getCustoms_fee(){
        return customs_fee;
    }

    public void setCustoms_fee(double customs_fee){
        this.customs_fee = customs_fee;
    }

    public String price_tag() {
        return getName() + " $ " + total_price() + " (Customs fee: $ " + customs_fee + ")";
    }

    public double total_price() {
        return getPrice() + getCustoms_fee();
    }
}
