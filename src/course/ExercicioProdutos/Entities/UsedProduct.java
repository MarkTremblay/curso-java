package course.ExercicioProdutos.Entities;

public class UsedProduct extends Product {

    private String manufactured_date;

    public UsedProduct() {}

    public UsedProduct(String name, double price, String manufactured_date){
        super(name, price);
        this.manufactured_date = manufactured_date;
    }

    public String getManufactured_date(){
        return manufactured_date;
    }

    public void setManufactured_date(String manufactured_date){
        this.manufactured_date = manufactured_date;
    }

    @Override
    public String price_tag(){
        return getName() + " (used) $ " + getPrice() + " (Manufacture date: " + manufactured_date + ")";
    }
}
