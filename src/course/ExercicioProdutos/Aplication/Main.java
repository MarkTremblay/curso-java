package course.ExercicioProdutos.Aplication;

import course.ExercicioProdutos.Entities.ImportedProduct;
import course.ExercicioProdutos.Entities.Product;
import course.ExercicioProdutos.Entities.UsedProduct;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args){

        System.out.print("Enter the number of products: ");
        int entrada = in.nextInt();

        ArrayList<Product> lista = new ArrayList <>();

        for (int i = 1; i <= entrada; i++) {

            System.out.printf("Product #%d data: ",i);
            System.out.print("\nCommon, used or imported (c/u/i)?");
            char tipo = in.next().charAt(0);
            in.nextLine();

            String name;
            double price;
            String date;
            double customs_fee;

            switch ( tipo ) {
                case 'c':
                    System.out.print("Name: ");
                    name = in.nextLine();

                    System.out.print("Price: ");
                    price = in.nextDouble();

                    Product product = new Product(name, price);
                    lista.add(product);

                    break;

                case 'u':
                    System.out.print("Name: ");
                    name = in.nextLine();

                    System.out.print("Price: ");
                    price = in.nextDouble();

                    in.nextLine();
                    System.out.print("Manufacture date (DD/MM/YYYY): ");
                    date = in.nextLine();

                    UsedProduct usedProduct = new UsedProduct(name, price, date);
                    lista.add(usedProduct);

                    break;

                case 'i':

                    System.out.print("Name: ");
                    name = in.nextLine();

                    System.out.print("Price: ");
                    price = in.nextDouble();

                    System.out.print("Customs fee: ");
                    customs_fee = in.nextDouble();

                    ImportedProduct importedProduct = new ImportedProduct(name, price, customs_fee);
                    lista.add(importedProduct);

                    break;

                    default:
                        System.out.println("\nDigite uma entrada válida.");
                        i--;
            }
        }

        System.out.println("\nPRICE TAGS");
        for (Product c : lista) {
            System.out.println(c.price_tag());
        }
    }
}
