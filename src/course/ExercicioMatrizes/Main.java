package course.ExercicioMatrizes;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Digite a quantidade de linhas e colunas: ");
        int linhas = in.nextInt();
        int colunas = in.nextInt();

        int[][] matriz = new int[linhas][colunas];

        System.out.println("Digite os valores: ");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = in.nextInt();
            }
        }

        int numero = in.nextInt();

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if(matriz[i][j] == numero) {
                    System.out.println("\nPosição: " + "(" + i + ", " + j + ")");

                    if (i > 0) {
                        System.out.println("Acima: "+matriz[i-1][j]);
                    }
                    if (i < linhas-1) {
                        System.out.println("Abaixo: "+matriz[i+1][j]);
                    }
                    if (j > 0) {
                        System.out.println("Esquerda: "+matriz[i][j-1]);
                    }
                    if (j < colunas-1) {
                        System.out.println("Direita: "+matriz[i][j+1]);
                    }
                }
            }
        }

        in.close();
    }
}
